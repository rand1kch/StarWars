//
//  FilmService.swift
//  StarWars
//
//  Created by Даниил Тимонин on 16.02.2023.
//

import Foundation
import Alamofire

class FilmService {
    
    func getFilms() async throws -> [Film] {
        let response = await AF.request("https://swapi.dev/api/films")
            .serializingDecodable(Films.self)
            .response
        return try response.result.get().all
    }
}
