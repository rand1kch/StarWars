import SwiftUI
import StarWarsUI

@main
struct StarWarsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
