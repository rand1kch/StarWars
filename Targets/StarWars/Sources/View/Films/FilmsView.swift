//
//  FilmsView.swift
//  StarWars
//
//  Created by Даниил Тимонин on 16.02.2023.
//

import SwiftUI

struct FilmsView: View {
    @ObservedObject var viewModel = FilmsViewModel()
    
    var body: some View {
        ScrollView (showsIndicators: false){
            LazyVStack {
                if viewModel.isLoading {
                    ProgressView()
                } else {
                    ForEach(viewModel.films) { film in
                        Text(film.producer)
                    }
                }
            }
        }.onAppear{
            viewModel.loadInitialData()
        }
    }
}

struct FilmsView_Previews: PreviewProvider {
    static var previews: some View {
        FilmsView()
    }
}
