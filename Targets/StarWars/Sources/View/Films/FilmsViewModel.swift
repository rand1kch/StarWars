//
//  FilmsViewModel.swift
//  StarWars
//
//  Created by Даниил Тимонин on 16.02.2023.
//

import Foundation

class FilmsViewModel: ObservableObject {
    
    private let service: FilmService
    
    @Published var isLoading: Bool = false
    @Published var films: [Film] = []
    
    init() {
        self.service = FilmService()
    }
    
    func loadInitialData() {
        Task {
            self.isLoading = true
            do {
                let response = try await service.getFilms()
                films = response
            } catch {
                print(error)
            }
            self.isLoading = false
        }
    }
    
}
