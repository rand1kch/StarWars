import SwiftUI

public struct ContentView: View {
    public init() {}
    
    public var body: some View {
        NavigationView {
            VStack{
                NavigationLink("Films", destination: { FilmsView() })
            }
        }.navigationBarTitle(Text("Screens"))
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
