//
//  StarWarsApp.swift
//  StarWars
//
//  Created by Даниил Тимонин on 16.02.2023.
//

import SwiftUI

@main
struct StarWarsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
