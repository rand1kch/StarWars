import ProjectDescription

let dependencies = Dependencies(
    swiftPackageManager: [
            .package(url: "https://github.com/ReSwift/ReSwift.git", .upToNextMajor(from: "6.1.1")),
            .package(url: "https://github.com/ReSwift/ReSwift-Thunk.git", .upToNextMajor(from: "2.0.0")),
            .package(url: "https://github.com/Swinject/Swinject.git", .upToNextMajor(from: "2.8.0")),
            .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.6.4"))
    ],
    platforms: [.iOS]
)
