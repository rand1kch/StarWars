import ProjectDescription

/// Project helpers are functions that simplify the way you define your project.
/// Share code to create targets, settings, dependencies,
/// Create your own conventions, e.g: a func that makes sure all shared targets are "static frameworks"
/// See https://docs.tuist.io/guides/helpers/

extension Project {
    /// Helper function to create the Project for this ExampleApp
    public static func app(name: String, targetVersion: String, platform: Platform, additionalTargets: [String]) -> Project {
        let externalDependencies = [
            TargetDependency.external(name: "ReSwift"),
            TargetDependency.external(name: "ReSwiftThunk"),
            TargetDependency.external(name: "Swinject"),
            TargetDependency.external(name: "Alamofire")
        ]
        var targets = makeAppTargets(name: name,
                                     targetVersion: targetVersion,
                                     platform: platform,
                                     dependencies: additionalTargets.map { TargetDependency.target(name: $0) } + externalDependencies)
        targets += additionalTargets.flatMap({ makeFrameworkTargets(name: $0, targetVersion: targetVersion, platform: platform) })
        return Project(name: name,
                       targets: targets)
    }

    // MARK: - Private

    /// Helper function to create a framework target and an associated unit test target
    private static func makeFrameworkTargets(name: String, targetVersion: String, platform: Platform) -> [Target] {
        let sources = Target(name: name,
                platform: platform,
                product: .framework,
                bundleId: "ru.itrocket.\(name)",
                deploymentTarget: .iOS(targetVersion: targetVersion, devices: [.iphone]),
                 infoPlist: .default,
                sources: ["Targets/\(name)/Sources/**"],
                resources: ["Targets/\(name)/Resources/**"],
                dependencies: []
        )
        return [sources]
    }

    /// Helper function to create the application target and the unit test target.
    private static func makeAppTargets(name: String, targetVersion: String, platform: Platform, dependencies: [TargetDependency]) -> [Target] {
        let platform: Platform = platform
        let infoPlist: [String: InfoPlist.Value] = [
            "CFBundleShortVersionString": "1.0.0",
            "CFBundleVersion": "6",
            "UILaunchScreen": [
                "UIColorName": "LaunchScreenBackgroundColor",
                "UIImageName": "LaunchScreenImage",
                "UIImageRespectsSafeAreaInsets": "true"
            ],
            "UIApplicationSceneManifest": [
                "UIApplicationSupportsMultipleScenes": "false",
                "UISceneConfigurations": [
                    "UIWindowSceneSessionRoleApplication": [[
                        "UISceneConfigurationName": "Default Configuration",
                        "UISceneDelegateClassName": "$(PRODUCT_MODULE_NAME).SceneDelegate"
                    ]]
                ]
            ],
            "UIUserInterfaceStyle": "Light"
        ]
        
        let mainTarget = Target(
            name: name,
            platform: platform,
            product: .app,
            bundleId: "ru.itrocket.starwarsapp",
            deploymentTarget: .iOS(targetVersion: targetVersion, devices: [.iphone]),
            infoPlist: .extendingDefault(with: infoPlist),
            sources: ["Targets/\(name)/Sources/**"],
            resources: ["Targets/\(name)/Resources/**"],
            dependencies: dependencies
        )

        return [mainTarget]
    }
}
